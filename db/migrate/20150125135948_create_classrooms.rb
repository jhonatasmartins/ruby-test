class CreateClassrooms < ActiveRecord::Migration

    def up
		create_table :classrooms do |t|
			t.belongs_to :course,  index: true
			t.belongs_to :student, index: true

			t.timestamps null: false
		end
	end

	def down
		drop_table :classrooms
	end

end
