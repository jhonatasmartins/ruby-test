class Course < ActiveRecord::Base
	has_many :classrooms
	has_many :students, :through => :classrooms

	has_enumeration_for :status, :with => CourseStatus

	validates_presence_of :name, :description, :status
	validates_length_of :name, maximum: 45, allow_blank: false
	validates_length_of :description, maximum: 45, allow_blank: false

end

