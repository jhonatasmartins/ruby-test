class Student < ActiveRecord::Base
	has_many :classrooms
	has_many :courses,  :through => :classrooms

	has_enumeration_for :status, :with => StudentStatus

	validates_presence_of :name, :register_number, :status
	validates_length_of :name, maximum: 45, allow_blank: false
	validates_length_of :register_number, maximum: 45, allow_blank: false

end
