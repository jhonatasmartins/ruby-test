class CourseStatus < EnumerateIt::Base
	associate_values(
		:opened => 1,
		:progress => 2,
		:closed => 3
	)
end