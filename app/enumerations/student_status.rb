class StudentStatus < EnumerateIt::Base
	associate_values(
		:registered => 1,
		:attending => 2
	)
end