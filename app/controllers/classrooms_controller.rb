class ClassroomsController < ApplicationController

	def index
		@classrooms = Classroom.take(5)
	end

	def new
		@classroom = Classroom.new
	end

	def create
		@classroom = Classroom.new(classroom_params)

		if @classroom.save
			redirect_to action: "index", notice: "Created with successful"
		else
			render action: :new
		end
	end

	def destroy
		@classroom = Classroom.find(params[:id])

		if @classroom.delete
			redirect_to action: "index", notice: "Deleted with successful"
		else
			redirect_to action: "index", notice: "Deleted was failed"
		end

	end

	private

	def classroom_params
		params.require(:classroom).permit(:course_id, :student_id)
	end


end