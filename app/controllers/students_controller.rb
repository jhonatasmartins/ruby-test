class StudentsController < ApplicationController

	def index
		@students = Student.all
	end

	def new
		@student = Student.new
	end

	def create
		@student = Student.new(student_params)

		if @student.save
			redirect_to @student, notice: "Created with successful"
		else
			render action: :new
		end
	end

	def show
		@student = Student.find(params[:id])
	end

	def edit
		@student = Student.find(params[:id])
	end

	def update
		@student = Student.find(params[:id])

		if @student.update(student_params)
			redirect_to @student, notice: "Updated with successful"
		else
			render action: :edit
		end
	end

	private 

	def student_params
		params.require(:student).permit(:name, :register_number, :status)
	end

end