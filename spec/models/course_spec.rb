require 'spec_helper'

describe Course do
    it "course should be invalid" do
		Factory.build(:course, name:nil).should_not be_valid
	end
end
